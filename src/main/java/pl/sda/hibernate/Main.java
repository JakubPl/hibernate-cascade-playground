package pl.sda.hibernate;

import org.hibernate.SessionFactory;
import pl.sda.hibernate.entity.Notebook;
import pl.sda.hibernate.entity.Student;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        final EntityManager entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();

        final Student student = new Student();
        student.setName("Jakub Plonka");

        List<Notebook> notebooks = Arrays.asList(new Notebook("model1", student), new Notebook("model2", student));

        student.setNotebooks(notebooks);

        entityManager.persist(student);
        //entityManager.persist(notebook);

        entityManager.getTransaction().commit();


        //mimo, ze nie wywolalismy jawnie persist na notebooks to dzieki
        // cascade i wywolaniu entityManager.persist(student) hibernate wywolal za nas persist na wszystkich laptopach, ktore ma student
        List<Notebook> notebooksFromDb = entityManager.createQuery("FROM Notebook", Notebook.class).getResultList();

        notebooksFromDb.forEach(System.out::println);

        HibernateUtils.shutdown();


    }
}
